package com.project;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by zeru on 08/03/16.
 */
public class FileManager {

    public String checkActualId(){
        String line = null;
        Path file = Paths.get("ID_Actual");
        try (InputStream in = Files.newInputStream(file);
             BufferedReader reader =
                     new BufferedReader(new InputStreamReader(in))) {
            line = reader.readLine();
        } catch (IOException x) {
            System.err.println(x);
        }
        return line;
    }

    public void updateActualId(){
        int id = Integer.parseInt(checkActualId());
        id++;
        System.out.println("Current id is: "+id);
        write(String.valueOf(id),"ID_Actual");
    }

    public void makeDir(){
        new File(checkActualId()).mkdir();
    }

    public void write(String content, String path){
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
