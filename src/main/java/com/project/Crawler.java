package com.project;

/**
 * Created by zeru on 08/03/16.
 */
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Crawler {
    /**
     *
     * @param url
     */
    public void fetchData(String url) {

        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileManager fileManager = new FileManager();

        fileManager.updateActualId();
        fileManager.makeDir();
        fileManager.write(doc.toString(),fileManager.checkActualId()+"/content");

        Elements links = doc.select("a[href]");
        fileManager.write(links.attr("abs:href"),fileManager.checkActualId()+"/links");
        String Links = "";
        for (Element link : links) {
            //print(" * a: <%s>  (%s)", link.attr("abs:href"));
            Links += link.attr("abs:href") + "\n";

        }
        fileManager.write(Links,fileManager.checkActualId()+"/links");
    }

}
