package com.project;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.response.GetUpdatesResponse;

import java.util.List;

/**
 * Created by zeru on 08/03/16.
 */
public class BotManager {

    int latestMsgId = -1;
    List<Update> updates;
    String chatId;
    String url;
    int msgId;
    TelegramBot bot;

    public BotManager(TelegramBot pBot){
        bot = pBot;
    }

    void start(){
        while (true){
            getMessage();
        }
    }

    void getMessage(){
        GetUpdatesResponse updatesResponse = bot.getUpdates(-1,1,0);
        updates = updatesResponse.updates();
        if (updates.size() != 0){
            Message message = updates.get(0).message();
            chatId = message.chat().id().toString();
            url = message.text();
            msgId = message.messageId();
            if(msgId != latestMsgId) {
                System.out.println("Got a new message:");
                System.out.println(url);
                if (url.contains("http://") || url.contains("https://")){
                    Crawler crawler = new Crawler();
                    crawler.fetchData(url);
                    sendMessage("Received URL: " + url);
                }else{
                    sendMessage("Invalid URL");
                }
                    latestMsgId = msgId;
            }
        }
    }

    void sendMessage(String pMessage){
        bot.sendMessage(chatId, pMessage);
    }
}
